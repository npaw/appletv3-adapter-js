var path = require('path')

module.exports = {
  entry: './src/sp.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'sp.min.js',
    library: 'youbora',
    libraryTarget: 'umd'
  },
  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  }
}
