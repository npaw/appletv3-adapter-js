var youbora = require('youboralib')

var workarounds = {
  begin: function (plugin) {
    var req = new youbora.Request(plugin.getHost(), youbora.Constants.Service.DATA, {
      apiVersion: 'v7',
      outputformat: 'json',
      pluginVersion: plugin.getAdapter().getVersion(),
      system: plugin.getAccountCode()
    })
    req.getXHR().onreadystatechange = function () {
      if (req.getXHR().readyState == 4) {
        plugin.restartViewTransform(req.getXHR().responseText)
      }
    }
    req.send()
  }
}

module.exports = workarounds
