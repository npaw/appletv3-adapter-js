var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.AppleTV3 = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = null
    try {
      if (this.player && this.player.currentItem) {
        ret = this.player.currentItem.duration
      } else if (this.player && this.player.currentMediaItemDuration) {
        ret = this.player.currentMediaItemDuration
      } else if (typeof atv !== 'undefined') {
        ret = atv.player.currentItem.duration
      }
    } catch (err) {
    // nothing
    }
    return ret
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Apple TV'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    // this.monitorPlayhead(true, false)
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    // if (this.monitor) this.monitor.stop()
  },

  didStopPlaying: function () {
    this.fireStop()
  },

  playerStateChanged: function (newState, playhead) {
    this._updatePlayhead(playhead)
    switch (newState.toLowerCase()) {
      case 'playing':
        this.fireJoin()
        this.fireSeekEnd()
        this.fireBufferEnd()
        this.fireResume()
        break
      case 'paused':
        this.firePause()
        break
    }
  },

  onStartBuffering: function (playhead) {
    this._updatePlayhead(playhead)
    if (!this.flags.isStarted) {
      this.fireStart({ referer: this.referer })
    } else if (!this.flags.isSeeking) {
      this.fireBufferBegin()
    }
  },

  onBufferSufficientToPlay: function () {
  },

  playerWillSeekToTime: function (playhead) {
    this._updatePlayhead(playhead)
    this.fireSeekBegin()
  },

  onPlaybackError: function (msg) {
    this.fireError(msg)
  },

  playerTimeDidChange: function (playhead) {
    this._updatePlayhead(playhead)
  },

  begin: function () {
    youbora.adapters.AppleTV3.AppleWorkarounds.begin(this.plugin)
  },

  _updatePlayhead: function (ph) {
    this.playhead = Math.round(ph * 1000) / 1000
    if (this.playhead >= 1) this.fireJoin()
  }
},
{
  AppleWorkarounds: require('./appletvWorkarounds')
}
)

module.exports = youbora.adapters.AppleTV3
