## [6.5.3] 2020-11-25
### Added
- Multiple event check for timeupdate to trigger join event

## [6.5.2] 2020-10-05
### Fixed
- Player name changed from `Apple TV 3` to `Apple TV`
### Added
- Case for `getDuration` with `currentMediaItemDuration` player property

## [6.5.1] 2020-06-29
### Fixed
- Try catch for getDuration
- Case insensitive for playerStateChanged

## [6.5.0] 2019-07-17
### Library
- Packaged with `lib 6.5.7`

